package com.b2b.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.HashMap;
import java.util.Map;

/**
 * @author vkumar 1/15/2017
 */
@Component
public class LoginFormValidator implements Validator {

    @Override
    public void validate(final Object obj, final Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "id", "id.required");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "username is required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "password is required");
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return true;
    }
}
