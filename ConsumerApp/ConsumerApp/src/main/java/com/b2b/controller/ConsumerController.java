package com.b2b.controller;

import com.b2b.model.Consumer;
import com.b2b.service.ConsumerService;
import com.b2b.transformer.ConsumerTransformer;
import com.b2b.validator.LoginFormValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

/**
 * Created by vkumar on 1/7/2017.
 */

@Controller
public class ConsumerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerController.class);

    @Autowired
    private ConsumerTransformer consumerRequestTransformer;

    @Autowired
    private ConsumerService consumerService;

    @Autowired
    private LoginFormValidator loginFormValidator;

    @RequestMapping(value = "/storeConsumerInfo", method = RequestMethod.GET)
    public ModelAndView storeConsumer() {
        final String message =
            consumerService.storeConsumerInfo(consumerRequestTransformer.transform(createConsumer()));
        LOGGER.info("Store consumer service response : {}", message);
        return new ModelAndView("RequestForm").addObject("message", message);
    }

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public ModelAndView goToSignUp() {
        return new ModelAndView("Signup");
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView goToLogin() {
        return new ModelAndView("Login");
    }

    @RequestMapping(value = "/authenticateLogin", method = RequestMethod.GET)
    public ModelAndView authenticateLogin(
        @RequestParam(required = true) final String username,
        @RequestParam(required = true) final String password, final BindingResult errors) {
        return new ModelAndView("RequestForm", "user", username);
    }

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public ModelAndView index() {
        return new ModelAndView("index", "message", "Welcome to the B2B consumer site!!!");
    }

    private Consumer createConsumer() {
        final Consumer consumer = new Consumer();
        consumer.setFirstName("Wesley");
        consumer.setLastName("Hunter");
        consumer.setNickName("Heaven");
        consumer.setEmail("wesley5@gmail.com");
        consumer.setFlags(1);
        consumer.setMobile("569841244");
        consumer.setRole("Buyer");
        consumer.setDob(new Date());
        return consumer;
    }
}


