package com.b2b.controller;

import com.b2b.model.Consumer;
import com.b2b.service.ConsumerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author vkumar 1/7/2017.
 */
@RestController
public class ConsumerServiceController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerServiceController.class);

    @Autowired
    private final ConsumerService consumerService;

    @Autowired
    public ConsumerServiceController(
        final ConsumerService consumerService) {
        this.consumerService = consumerService;
    }

    @RequestMapping(
        value = "/createUser",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        method = RequestMethod.POST)
    public ResponseEntity<String> createUser(@RequestBody final Consumer consumer) {
        LOGGER.debug("Got the user creation request : {}", consumer.toString());
        final int existingUserId = consumerService.checkExistingUser(consumer.getEmail(), consumer.getMobile());

        if (existingUserId != 0) {
            return ResponseEntity.status(HttpStatus.FOUND).body(existingUserId + "is an existing user.");
        }
        final String body = "User account " + consumerService.save(consumer) + " successfully created";
        return ResponseEntity.status(HttpStatus.CREATED).body(body);
    }

    @RequestMapping(
        value = "/getUserInfo", produces = MediaType.APPLICATION_JSON_VALUE,
        method = RequestMethod.GET)
    public ResponseEntity<Consumer> getConsumerInfo(@RequestParam final int id) {
        LOGGER.info("Get the consumer from service!!! {}", id);
        return ResponseEntity.status(HttpStatus.OK).body(consumerService.getConsumer(id));
    }
}
