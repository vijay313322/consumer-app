package com.b2b.service;

import com.b2b.dao.ConsumerDAO;
import com.b2b.model.Consumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author vkumar 5/1/2017
 */
@Service
public class ConsumerService {

    private final ConsumerDAO consumerDAO;

    @Autowired
    public ConsumerService(final ConsumerDAO consumerDAO) {
        this.consumerDAO = consumerDAO;
    }

    public int save(final Consumer consumer) {
        return consumerDAO.create(consumer);
    }

    public int checkExistingUser(final String email, final String mobile) {
        return consumerDAO.checkExistingUser(email, mobile);
    }

    public Consumer getConsumer(final int id) {
        return consumerDAO.getConsumer(id);
    }
}
