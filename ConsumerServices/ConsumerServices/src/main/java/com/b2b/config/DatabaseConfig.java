package com.b2b.config;

import com.b2b.dao.JDBCConsumerDAO;
import com.b2b.dao.ConsumerDAO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author vkumar 02/02/2017
 */

@Configuration
public class DatabaseConfig {

    @Bean
    public ConsumerDAO userDAO() {
        return new JDBCConsumerDAO();
    }
}
