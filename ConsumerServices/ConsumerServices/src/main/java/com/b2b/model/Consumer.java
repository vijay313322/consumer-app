package com.b2b.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.text.ParseException;
import java.util.Date;

;

/**
 * The bean class for Consumer
 * Created by vkumar on 1/7/2017.
 */
public class Consumer {

    private final int id;

    private final String firstName;
    private final String lastName;
    private final String nickName;
    private final String email;
    private final Date dateOfBirth;
    private final int status;
    private final int flags;
    private final String mobile;
    private final int role;
    private final String credential;

    public String getCredential() {
        return credential;
    }

    public Consumer(
        @JsonProperty("id") final int id,
        @JsonProperty("mobile") final String mobile,
        @JsonProperty("FirstName") final String firstName,
        @JsonProperty("LastName") final String lastName,
        @JsonProperty("NickName") final String nickName,
        @JsonProperty("Email") final String email,
        @JsonProperty("dob") final Date dob,
        @JsonProperty("status") final int status,
        @JsonProperty("role") final int role,
        @JsonProperty("flags") final int flags,
        @JsonProperty("credential") final String credential) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.nickName = nickName;
        this.email = email;
        this.dateOfBirth = new Date(dob.getTime());
        this.status = status;
        this.flags = flags;
        this.mobile = mobile;
        this.role = role;
        this.credential = credential;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getNickName() {
        return nickName;
    }

    public String getEmail() {
        return email;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public int getStatus() {
        return status;
    }

    public int getFlags() {
        return flags;
    }

    public String getMobile() {
        return mobile;
    }

    public int getRole() {
        return role;
    }

    public int getId() {
        return id;
    }

}
