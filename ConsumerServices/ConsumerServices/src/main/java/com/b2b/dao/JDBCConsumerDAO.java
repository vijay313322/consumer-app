package com.b2b.dao;

import com.b2b.model.Consumer;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.util.Date;

/**
 * @author vkumar 5/1/2017
 */
public class JDBCConsumerDAO implements ConsumerDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(JDBCConsumerDAO.class);

    @Autowired
    private NamedParameterJdbcTemplate parameterJdbcTemplate;

    private static final String CREATE_EMPLOYEE_INFO =
        "INSERT INTO consumers.consumer (firstname, lastname, nickname, email, date_of_birth, status, \n"
            + "            flags, time_created, time_updated, time_last_login, primary_contact_id, \n"
            + "            mobile, role) VALUES "
            + "(:firstName, :lastName, :nickName, :email, :dob,:status,:flags, :time_created, '', "
            + "'', '', :mobile, :role)";

    private static final String CREATE_INTERNAL_USER_CREDENTIAL =
        "INSERT INTO consumers.user_internal_credential (id, consumer_id, credential) VALUES \n"
            + "(:consumer_id, :credential)";

    private static final String SELECT_USER_BY_MOBILE_OR_EMAIL =
        "select * from consumers.consumer where mobile=:mobile or email=:email ";

    private static final String SELECT_USER_BY_ID =
        "select * from consumers.consumer where id=:id";

    private final RowMapper<Consumer> rowMapper = new ConsumerRowMapper();

    @Override
    public int create(final Consumer consumer) {
        final KeyHolder keyHolder = new GeneratedKeyHolder();

        parameterJdbcTemplate.update(
            CREATE_EMPLOYEE_INFO,
            new MapSqlParameterSource()
                .addValue(JDBCConstants.FIRST_NAME, consumer.getFirstName())
                .addValue(JDBCConstants.LAST_NAME, consumer.getLastName())
                .addValue(JDBCConstants.NICK_NAME, consumer.getNickName())
                .addValue(JDBCConstants.EMAIL, consumer.getEmail())
                .addValue(JDBCConstants.DOB, consumer.getDateOfBirth())
                .addValue(JDBCConstants.MOBILE, consumer.getMobile())
                .addValue(JDBCConstants.ROLE, consumer.getRole())
                .addValue(JDBCConstants.STATUS, consumer.getStatus())
                .addValue(JDBCConstants.FLAGS, consumer.getFlags())
                .addValue(JDBCConstants.TIME_CREATED, new Date())
                .addValue(JDBCConstants.PRIMARY_CONCAT_ID, 123),
            keyHolder,
            new String[]{"id"}
        );

        final int consumerId = keyHolder.getKey().intValue();

        parameterJdbcTemplate.update(
            CREATE_INTERNAL_USER_CREDENTIAL,
            new MapSqlParameterSource()
                .addValue(JDBCConstants.CREDENTIAL, consumer.getCredential())
                .addValue(JDBCConstants.CONSUMER_ID, consumerId),
            keyHolder,
            new String[]{"id"});

        return keyHolder.getKey().intValue();

    }

    @Override
    public int checkExistingUser(final String email, final String mobile) {
        try {
            final Consumer consumer = parameterJdbcTemplate.queryForObject(
                SELECT_USER_BY_MOBILE_OR_EMAIL,
                ImmutableMap.of(JDBCConstants.MOBILE, mobile, JDBCConstants.EMAIL, email),
                rowMapper);
            return consumer.getId();
        } catch (final EmptyResultDataAccessException emptyResultDataAccessException) {
            LOGGER.error("No  existing user found");
            return 0;
        }
    }

    @Override
    public Consumer getConsumer(final int id) {
        try {
            final Consumer consumer = parameterJdbcTemplate.queryForObject(
                SELECT_USER_BY_ID,
                ImmutableMap.of(JDBCConstants.ID, id),
                rowMapper);
            return consumer;
        } catch (final EmptyResultDataAccessException emptyResultDataAccessException) {
            LOGGER.error("No  existing user found");
            return null;
        }
    }
}
