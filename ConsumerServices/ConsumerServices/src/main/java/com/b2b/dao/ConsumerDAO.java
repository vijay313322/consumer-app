package com.b2b.dao;

import com.b2b.model.Consumer;

/**
 * @author vkumar 5/1/2017
 */
public interface ConsumerDAO {

    int create(Consumer consumer);

    int checkExistingUser(final String email, final String mobile);

    Consumer getConsumer(final int id);

}
