package com.b2b.dao;

import com.b2b.model.Consumer;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author vkumar 5/19/2017
 */
public class ConsumerRowMapper implements RowMapper<Consumer> {

    @Override
    public Consumer mapRow(final ResultSet rs, final int rowNum) throws SQLException {
        return new Consumer(
            rs.getInt("id"),
            rs.getString("mobile"),
            rs.getString("firstname"),
            rs.getString("lastname"),
            rs.getString("nickname"),
            rs.getString("email"),
            rs.getDate("date_of_birth"),
            rs.getInt("status"),
            rs.getInt("role"),
            rs.getInt("flags"));
    }
}
