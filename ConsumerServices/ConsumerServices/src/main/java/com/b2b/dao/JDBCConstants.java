package com.b2b.dao;

/**
 * @author vkumar 5/1/2017
 */
public class JDBCConstants {

    public static final String EMAIL = "email";
    public static final String MOBILE = "mobile";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String NICK_NAME = "nickName";
    public static final String DOB = "dob";
    public static final String ROLE = "role";
    public static final String STATUS = "status";
    public static final String FLAGS = "flags";
    public static final String TIME_CREATED = "time_created";
    public static final String TIME_UPDATED = "time_updated";
    public static final String TIME_LAST_LOGIN = "time_last_login";
    public static final String PRIMARY_CONCAT_ID = "primary_contact_id";
    public static final String ID = "id";
    public static final String CREDENTIAL = "credential";
    public static final String CONSUMER_ID = "consumer_id";

}
