package com.b2b;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class ConsumerServicesApplication {

    public static void main(final String[] args) {
        SpringApplication.run(ConsumerServicesApplication.class, args);
    }
}
